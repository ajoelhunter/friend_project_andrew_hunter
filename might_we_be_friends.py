# Andrew Hunter, purpose of code: 9/18/23 make a program to test how compatible people are with me
print("How compatible are you with me? Let's find out!")
score = 0
print("Please enter yes or no for the following questions.")
# program a process of grading the different questions
print("------")
print("Do you like to play video games?")
answer_1 = input()
if answer_1 == "Yes" or answer_1 == "yes":
    print("Great, me too!")
    score = score + 20
else:
    print("Bummer, video games are my favorite thing")
    score = score - 20
print("")
print("Do you like Burger King")
answer_2 = input()
if answer_2 == "Yes" or answer_2 == "yes":
    print("Great, me too!")
    score = score + 15
else:
    print("Not too big of a deal, try out their chicken!")
    score = score - 5
print("")
print("Do you like dogs?")
answer_3 = input()
if answer_3 == "Yes" or answer_3 == "yes":
    print("Great, me too!")
    score = score + 20
else:
    print("It's a crime to not like dogs!")
    score = score - 30
print("")
print("Are airplanes scary for you?")
answer_4 = input()
if answer_4 == "No" or answer_4 == "no":
    print("I grew up a military brat so me too!")
    score = score + 15
else:
    print("Not too big of a deal, traveling on place is fun though!")
    score = score - 10
print("")
print("Are you a fan of Pokemon?")
answer_5 = input()
if answer_5 == "Yes" or answer_5 == "yes":
    print("Great, me too, Zoroark is my favorite pokemon!")
    score = score + 30
else:
    print("I can understand but Pokemon is apart of everyone's childhood!")
    score = score - 15
print("")
print("Are kinder bueno the best chocolate bar?")
answer_6 = input()
if answer_6 == "Yes" or answer_6 == "yes":
    print("Good, you chose the correct option!")
    score = score + 20
else:
    print("No bar could ever come close to a kinder bueno!")
    score = score - 20
print("------")
print("You scored ", score, ", which means your compatibility with me is most likely: ", end='')
if score <= -100:
    print("extremely incompatible.")
elif score >= -99 and score <= -51:
    print("moderately incompatible.")
elif score >= -50 and score <= -1:
    print("probably incompatible.")
elif score >= 0 and score <= 49:
    print("probably compatible.")
elif score >= 50 and score <= 99:
    print("moderately compatible.")
elif score >= 100:
    print("extremely compatible.")
